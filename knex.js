const config = require("./knexConfig");
const knex = require('knex')(config);

module.exports = knex;