const express = require("express");
const knex = require("../knex");

const router = express.Router();

router.get("/", async (req,res) => {
    try {
        const selectRow = await knex.select().from('tareas');
        res.render("index", {
           arrayRow : selectRow 
        });
    } catch (error) {
       res.status(400).send(error);
    }


    
});

router.get("/registrarEntrada", (req,res) => {
    res.render("RegistrarEntrada");
});

router.post("/registrarEntrada", async (req,res) => {
    const {tituloInput, cuerpoInput} = req.body;
    const fechaInput = new Date();
    const fechaActual = `${fechaInput.getFullYear()}-${fechaInput.getMonth()}-${fechaInput.getDay()}`

    try {
        const insert = await knex('tareas').insert({
            titulo: tituloInput,
            cuerpo : cuerpoInput,
            fecha : fechaActual
        });
    } catch (error) {
        res.status(400).send(error);
    }

    res.redirect("/");

});

router.get("/delete/:idDelete", async (req, res) => {
    const id = req.params.idDelete;
    try {
        const deleteRow = await knex('tareas').where("id",id).del();
    } catch (error) {
        res.status(400).send(error);
    }
    res.redirect("/");
});

router.get("/update/:idUpdate", async (req, res) => {
    const id = req.params.idUpdate;
    try {
        const updateRow = await knex.select().from("tareas").where("id",id);
        const currentRow = updateRow[0];
        res.render("EditarEntrada", {
            filas : currentRow
        });
    } catch (error) {
        res.status(400).send(error);
    }
})

router.post("/update/:idUpdate", async (req,res) => {
    const id = req.params.idUpdate;
    const {tituloInput, cuerpoInput} = req.body;

    try {
        await knex('tareas').where("id", id).update({
            titulo : tituloInput,
            cuerpo : cuerpoInput
        });
    } catch (error) {
        res.status(400).send(error);     
    }

    res.redirect("/")
})

module.exports = router;