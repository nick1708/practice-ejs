const express = require("express");
const bodyParse = require("body-parser");
const dotEnv = require("dotenv");
const morgan = require("morgan");
const path = require("path");
const router = require("./controllers/route");

const app = express();

dotEnv.config({path: "./config.env"});

app.set('views', path.join(__dirname, "views"));
app.set('view engine', 'ejs');

app.use(morgan('dev'));
app.use(bodyParse.json());
app.use(bodyParse.urlencoded({extend:false}));//consulta
app.use("/",router );


app.listen(process.env.PORT || 3000, _ => {
    console.clear();
    console.log(`Server is running on port ${process.env.PORT}`);
})